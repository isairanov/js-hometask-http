'use strict';
function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch
  let options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(userInfo)
  };
  return fetch('/users', options)
    .then(response => {
      if (response.status < 300) {
        return Promise.resolve(response);
      }
      else {
        return Promise.reject(`${response.status} ${response.statusText}`);
      }
    })
    .then(response => response.json())
    .then(user => Promise.resolve(user.id));
}

function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
  return fetch(`users/${id}`)
    .then(response => {
      if (response.status < 300) {
        return Promise.resolve(response);
      }
      else {
        return Promise.reject(`${response.status} ${response.statusText}`);
      }
    })
    .then(response => response.json())
    .then(user => Promise.resolve(user));
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', `users`);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(userInfo));
    xhr.onload = function () {
      return resolve(JSON.parse(xhr.responseText).id);
    };
    xhr.onerror = function () {
      return reject(`${xhr.status} ${xhr.statusText}`);
    };
  });
}

function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `users/${id}`);
    xhr.send();
    xhr.onload = function () {
      return resolve(JSON.parse(xhr.responseText));
    };
    xhr.onerror = function () {
      return reject(`${xhr.status} ${xhr.statusText}`);
    };
  });
}

/**
  * @param {boolean} isFetch true=Fetch / false=XHR
  */
function checkWork(isFetch) {
  if (isFetch) {
    addUserFetch({ name: "Alice", lastname: "FetchAPI" })
      .then(userId => {
        console.log(`Был добавлен пользователь с userId ${userId}`);
        return getUserFetch(userId);
      })
      .then(userInfo => {
        console.log(`Был получен пользователь:`, userInfo);
      })
      .catch(error => {
        console.error(error);
      });
  }
  else {
    addUserXHR({ name: "Dodo", lastname: "XHR" })
      .then(userId => {
        console.log(`Был добавлен пользователь с userId ${userId}`);
        return getUserXHR(userId);
      })
      .then(userObj => {
        console.log(`Был получен пользователь: `, userObj);
      })
      .catch(error => {
        console.error(error);
      });
  }
}

checkWork(true);
checkWork(false); // вопрос: почему checkWork(true), который вызывает FletchAPI выполняется позже checkWork(XHR), хотя функция вызвана раньше. В консоле. Но id при этом в правильной последовательности